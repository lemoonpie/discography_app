# README

* Ruby version: 2.2.5

* Rails version: 5.1.6.1

* Configuration: bundle install

* Database creation: rake db:migrate

* Database initialization: rake db:seed

* Template: https://www.creative-tim.com/product/material-dashboard

