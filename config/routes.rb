Rails.application.routes.draw do
  root :to => "welcome#index"
  resources :artists
  resources :artists do
    resources :lps
    get   :new

  end
  resources :lps

end
