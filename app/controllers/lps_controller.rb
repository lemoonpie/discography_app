class LpsController < ApplicationController

	def index
		@Lps = Lp.all
	end

	def show
		@artist = Artist.find(params[:id])
	end

	def new
		@lp = Lp.new
	end
  
	def create
		@artist = Artist.find(params[:artist_id])
		@lp = @artist.lps.create(lp_params)
		redirect_to artist_path(@artist)
	end

	def edit
		@lp = Lp.find(params[:id])
	end    
			
	def update
		@lp = Lp.find(params[:id])
		@lp.update_attributes(lp_params)
		redirect_to :action => :index, :id => @lp.id
	
	end
  
	def destroy
		@lp = Lp.find(params[:id])
		@lp.destroy

		redirect_to :action => :index
	end

	private

	def lp_params
		params.require(:lp).permit(:name, :description)
	end
end
